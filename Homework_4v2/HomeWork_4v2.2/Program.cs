﻿using System;

namespace HomeWork_4v2._2
{
    class Program
    {
        static void Main(string[] args)
        {
            //Задание №2.1
            Console.Write("Введите количество строк : ");
            int n = int.Parse(Console.ReadLine());
            if (n > 25)
            {
                Console.WriteLine($"Число {n}, является слишком большим. Введите число поменьше:");
                n = int.Parse(Console.ReadLine());
            }
            else
            {
                if (n < 1)
                {
                    Console.WriteLine($"Число {n}, является слишком маленьким. Введите число поменьше:");
                    n = int.Parse(Console.ReadLine());
                }
            }

            int[][] triangle = new int[n][];

            triangle[0] = new int[] { 1 };

            for (int i = 1; i < triangle.Length; i++)
            {
                triangle[i] = new int[i + 1];
                for (int j = 0; j <= i; j++)
                {
                    if (j == 0 || j == i)
                        triangle[i][j] = 1;
                    else
                    {
                        triangle[i][j] = triangle[i - 1][j - 1] + triangle[i - 1][j];
                    }
                }
            }

            for (int i = 0; i < triangle.Length; i++)
            {
                Console.Write(" ");
                for (int j = 0; j < triangle[i].Length; j++)
                {
                    Console.Write(" ");
                    Console.Write($"{triangle[i][j],5}");
                }
                Console.WriteLine();
            }

            Console.ReadKey();
        }
    }
}
