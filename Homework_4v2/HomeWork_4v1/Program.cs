﻿using System;

namespace HomeWork_4v1
{
    class Program
    {
        

        public static long factorial(int n)
        {
            long x = 1;
            for (long i = 1; i <= n; i++)
            {
                x *= i;
            }
            return x;
        }

        static void Main(string[] args)
        {
            //Задание №2.2
            Console.Write("Введите количество строк : ");
            int n = int.Parse(Console.ReadLine());
            if (n > 25)
            {
                Console.WriteLine($"Число {n}, является слишком большим. Введите число поменьше:");
                n = int.Parse(Console.ReadLine());
            }
            else
            {
                if (n < 1)
                {
                    Console.WriteLine($"Число {n}, является слишком маленьким. Введите число поменьше:");
                    n = int.Parse(Console.ReadLine());
                }
            }
            string x = "  ";

            for (int i = 0; i < n; i++)
            {
                for (int c = 0; c <= (n - i); c++) 
                {
                    Console.Write(x);
                }
                for (int c = 0; c <= i; c++)
                {
                    Console.Write(x); 
                    Console.Write(factorial(i) / (factorial(c) * factorial(i - c))); 
                }
                Console.WriteLine();
                Console.WriteLine(); 
            }
            Console.ReadLine();
        }
    }
}
        
    

