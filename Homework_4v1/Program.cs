﻿using System;

namespace Homework_4v2
{
    class Program
    {
        static void Main(string[] args)
        {
            //Задание №1

            Random rand = new Random();
            decimal[,] matrix = new decimal[13, 3]; //Матрица учёта финансов
            int[] profit = new int[13];//Массив дохода
            int[] expenses = new int[13];// Массив расходов
            decimal[] bad = new decimal[12];// Массив прибыли от худшей к лудшей
            decimal[] revenue = new decimal[12];//Массив прибыли
            int count = 0;
            int p = 0;//Число прибыльных месяцев
            int a = 0;//Самый худший месяц по прибыли
            int b = 0;//Месяц занимающий второе месте с конца
            int c = 0;//Месяц занимающий третее месте с конца
            int d = 0;//Самая худшая прибыль
            int g = 1;//Прибыль занимающая второе место с конца
            int f = 2;//Прибыль занимающая третее место с конца
            //Заполнение массива с доходом
            for (int i = 0; i < profit.Length; i++)
            {
                profit[i] = rand.Next(30000, 100001);
            }
            //Заполнение массива с расходом
            for (int i = 0; i < expenses.Length; i++)
            {
                expenses[i] = rand.Next(0, 60001);
            }
            //Заполнение матрицы с финансовыми учётами
            for (int i = 1; i < matrix.GetLength(0); i++)
            {
                Console.Write($"{i,3} m.");
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (j == 0)//Столб дохода
                    {
                        matrix[i, j] = profit[i];
                    }
                    else
                    {
                        if (j == 1)//Столб расхода
                        {
                            matrix[i, j] = expenses[i];
                        }
                        else
                        {
                            if (j == 2)//Столб прибыли
                            {
                                matrix[i, j] = matrix[i, 0] - matrix[i, 1];

                                if (matrix[i, j] > 0) p++;

                                revenue[count++] = matrix[i, j];
                            }
                        }

                    }
                    Console.Write($"{matrix[i, j],6}  ");
                }
                Console.WriteLine();
            }
            Console.WriteLine($"Количество прибыльных месяцев = {p} ");

            for (int i = 0; i < revenue.Length; i++)
            {
                bad[i] = revenue[i];
            }
            Array.Sort(bad);
            //Нахождение худших месяцев
            for (int i = 0; i < revenue.Length; i++)
            {
                if (bad[d] == revenue[i]) a = i + 1;
                if (bad[g] == revenue[i]) b = i + 1;
                if (bad[f] == revenue[i]) c = i + 1;
            }
            Console.WriteLine($"Самые худшие месяцы: {a},{b},{c} ");
        }
    }
}
