﻿using System;

namespace HomeWork_4v3._3
{
    class Program
    {
        static int[,] Matrix3(int[,] a, int[,] b)
        {
            int[,] r = new int[a.GetLength(0), b.GetLength(1)];
            for (int i = 0; i < a.GetLength(0); i++)
            {
                for (int j = 0; j < b.GetLength(1); j++)
                {
                    for (int k = 0; k < b.GetLength(0); k++)
                    {
                        r[i, j] += a[i, k] * b[k, j];
                    }
                }
            }
            return r;
        }
       
        static void Print(int[,] a)
        {
            for (int i = 0; i < a.GetLength(0); i++)
            {
                for (int j = 0; j < a.GetLength(1); j++)
                {
                    Console.Write("{0} ", a[i, j]);
                }
                Console.WriteLine();
            }
        }
        static void Main(string[] args)
        {
            Random rand = new Random();
            int line = 0;//количество строк в первой матрице
            int column = 0;//количество столбцов в первой матрице
            int line2 = 0;//количество строк во второй матрице
            int column2 = 0;//количество столбцов во второй матрице
            Console.WriteLine("Введите количество строк первой матрицы:");
            line = int.Parse(Console.ReadLine());

            for (int i = 0; i < 100000; i++)
            {
                if (line < 0 || line == 0)
                {
                    Console.Write("Число строк не может быть отрицательным или равно 0. Повторите попытку");
                    line = int.Parse(Console.ReadLine());
                }
                else
                {
                    break;
                }
            }
            Console.WriteLine("Введите количество столбцов первой матрицы:");
            column = int.Parse(Console.ReadLine());
            for (int i = 0; i < 100000; i++)
            {
                if (column < 0 || column == 0)
                {
                    Console.Write("Число столбцов не может быть отрицательным или равно 0. Повторите попытку");
                    column = int.Parse(Console.ReadLine());
                }
                else
                {
                    break;
                }
            }
            Console.WriteLine("Введите количество строк второй матрицы:");
            line2 = int.Parse(Console.ReadLine());

            for (int i = 0; i < 100000; i++)
            {
                if (line2 < 0 || line2 == 0)
                {
                    Console.Write("Число строк не может быть отрицательным или равно 0. Повторите попытку");
                    line2 = int.Parse(Console.ReadLine());
                }
                else
                {                   
                  break;   
                }
            }
            Console.WriteLine("Введите количество столбцов второй матрицы:");
            column2 = int.Parse(Console.ReadLine());
            for (int i = 0; i < 100000; i++)
            {
                if (column2 < 0 || column2 == 0)
                {
                    Console.Write("Число столбцов не может быть отрицательным или равно 0. Повторите попытку");
                    column2 = int.Parse(Console.ReadLine());
                }
                else
                {   
                   break;
                }
            }
            int[,] matrix1 = new int[line, column];
            int[,] matrix2 = new int[line2, column2];
            for (int i = 0; i < line; i++)
            {
                for (int j = 0; j < column; j++)
                {
                    matrix1[i, j] = rand.Next(0, 10);
                    Console.Write($"{matrix1[i, j],3}");
                }
                Console.WriteLine();
            }
                Console.WriteLine("     *      ");  
            for (int i = 0; i < line; i++)
            {
                for (int j = 0; j < column; j++)
                {
                    matrix2[i, j] = rand.Next(0, 10);
                    Console.Write($"{matrix2[i, j],3}");
                }
                Console.WriteLine();
            }
            Console.WriteLine("     =      ");
            int[,] sum = Matrix3(matrix1, matrix2);
            Print(sum);
            Console.ReadKey();
        }
    }
}
