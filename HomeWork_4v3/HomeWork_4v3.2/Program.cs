﻿using System;

namespace HomeWork_4v3._2
{
    class Program
    {     
        static void Main(string[] args)
        {
            //Задание 3.2
            Random rand = new Random();
            int line = 0;//количество строк в первой матрице
            int column = 0;//количество столбцов в первой матрице
            int line2 = 0;//количество строк во второй матрице
            int column2 = 0;//количество столбцов во второй матрице
            Console.WriteLine("Введите количество строк первой матрицы:");
            line = int.Parse(Console.ReadLine());

            for (int i = 0; i < 100000; i++)
            {
                if (line < 0 || line == 0)
                {
                    Console.Write("Число строк не может быть отрицательным или равно 0. Повторите попытку");
                    line = int.Parse(Console.ReadLine());
                }
                else
                {
                    break;
                }
            }
                Console.WriteLine("Введите количество столбцов первой матрицы:");
                column = int.Parse(Console.ReadLine());
            for (int i = 0; i < 100000; i++)
            {
                if (column < 0 || column == 0)
                {
                    Console.Write("Число столбцов не может быть отрицательным или равно 0. Повторите попытку");
                    column = int.Parse(Console.ReadLine());
                }
                else
                {
                    break;
                }
            }
            Console.WriteLine("Введите количество строк второй матрицы:");
            line2 = int.Parse(Console.ReadLine());

            for (int i = 0; i < 100000; i++)
            {
                if (line2 < 0 || line2 == 0)
                {
                    Console.Write("Число строк не может быть отрицательным или равно 0. Повторите попытку");
                    line2 = int.Parse(Console.ReadLine());
                }
                else
                {
                    if (line != line2)
                    {
                        Console.Write("Количество строк первой матрицы должно быть равно количеству строк второй матрицы. Повторите попытку:");
                        line2 = int.Parse(Console.ReadLine());
                    }
                    else
                    {
                        break;
                    }
                }
            }
            Console.WriteLine("Введите количество столбцов второй матрицы:");
            column2 = int.Parse(Console.ReadLine());
            for (int i = 0; i < 100000; i++)
            {
                if (column2 < 0 || column2 == 0)
                {
                    Console.Write("Число столбцов не может быть отрицательным или равно 0. Повторите попытку");
                    column2 = int.Parse(Console.ReadLine());
                }
                else
                {
                    if (column != column2)
                    {
                        Console.Write("Количество столбцов первой матрицы должно быть равно количеству столбцов второй матрицы. Повторите попытку:");
                        column2 = int.Parse(Console.ReadLine());
                    }
                    else
                    {
                        break;
                    }
                }
            }
            int a = 0;
            Console.WriteLine("Введите положительное число, если вы желаете произвести сумму. Если вычитание то отрицательное:");
            a = int.Parse(Console.ReadLine());
            for (int i = 0; i>1000;i++)
            {
                if(a == 0)
                {
                    Console.WriteLine("Число 0 не отрицательное и не положительное. Повторите попытку:");
                    a = int.Parse(Console.ReadLine());
                }
                else
                {
                    break;
                }
            }
            int[,] matrix1 = new int[line,column];
            int[,] matrix2 = new int[line2,column2];
            int[,] sum = new int[line, column];
            int[,] difference = new int[line, column];
            for (int i = 0; i < line; i++)
            {
                for (int j = 0; j < column; j++)
                {
                    matrix1[i, j] = rand.Next(0, 10);
                    Console.Write($"{matrix1[i, j],3}");
                }
                Console.WriteLine();
            }
            if (a > 0)
            {
                Console.WriteLine("     +      ");
            }
            else
            {
                if(a<0)
                {
                    Console.WriteLine("     -      ");
                }
            }
            for (int i = 0; i < line; i++)
            {
                for (int j = 0; j < column; j++)
                {
                    matrix2[i, j] = rand.Next(0, 10);
                    Console.Write($"{matrix2[i, j],3}");
                }
                Console.WriteLine();
            }
            Console.WriteLine("     =      ");
            if (a > 0)
            {
                for (int i = 0; i < line; i++)
                {
                    for (int j = 0; j < column; j++)
                    {
                        sum[i, j] = matrix1[i, j] + matrix2[i, j];
                        Console.Write($"{sum[i, j],3}");
                    }
                    Console.WriteLine();
                }
            }
            else
            {
                if(a<0)
                {
                    for (int i = 0; i < line; i++)
                    {
                        for (int j = 0; j < column; j++)
                        {
                            difference[i, j] = matrix1[i, j] - matrix2[i, j];
                            Console.Write($"{difference[i, j],3}");
                        }
                        Console.WriteLine();
                    }
                }
            }
            Console.ReadKey();
        }                 
    }
}
