﻿ using System;

namespace HomeWork_4v3._1
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rand = new Random();
            int line = 0;//количество строк в матрице
            Console.WriteLine("Введите количество строк матрицы:");
            line = int.Parse(Console.ReadLine());
            int column = 0;//количество столбцов в матрице
            for (int i = 0; i < 100000; i++)
            {
                if (line < 0 || line == 0)
                {
                    Console.Write("Число строк не может быть отрицательным или равно 0. Повторите попытку");
                    line = int.Parse(Console.ReadLine());
                }
                else
                {
                    break;
                }
            }
            Console.WriteLine("Введите количество столбцов первой матрицы:");
            column = int.Parse(Console.ReadLine());
            for (int i = 0; i < 100000; i++)
            {
                if (column < 0 || column == 0)
                {
                    Console.Write("Число столбцов не может быть отрицательным или равно 0. Повторите попытку");
                    column = int.Parse(Console.ReadLine());
                }
                else
                {
                    break;
                }
            }
            int[,] matrix = new int[line, column];
            int[,] matrix2= new int[line, column];
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    matrix[i, j] = rand.Next(0, 10);
                    Console.Write($"{matrix[i, j],5}");
                }
                Console.WriteLine();
            }
            Console.WriteLine();
            int n = 0;
            Console.WriteLine("Введите число на которое будет умножаться матрица:");
            n = int.Parse(Console.ReadLine());
            for (int i = 0; i < line; i++)
            {
                for (int j = 0; j < column; j++)
                {
                    matrix2[i, j] = matrix[i, j] * n;
                    Console.Write($"{ matrix2[i, j],5} ");
                }
                Console.WriteLine();
            }
            Console.ReadKey();
        }
    }
}
